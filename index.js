import express from "express";
import fs from "fs";
const app = express();
const domain = "https://dosenbachlab.wustl.edu/services";
const port = 3000;

// middleware
app.use(express.json());
app.use("/services", express.static("public"));

// links
app.get("/services", (req, res) => {
    res.send("This message means we can see dosenbachlab services!");
});
app.post("/services/json", (req, res) => {
    console.log(req.body);
    try {
        let filename = req.body.filename;
        let data = req.body.data;
        fs.writeFileSync(filename, JSON.stringify(data));
    } catch (err) {
        console.error(err);
    }
    res.json(req.body);
});

// start application
app.listen(port, ()=> {
    console.log(`Server on ${domain}`);
});
